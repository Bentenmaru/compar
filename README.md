## Comparateur de similarités   

Petit script python pour comparer 2 fichiers textes et trouver les similarités, par exemple dans le but de trouver des fonctions copiées collées dans 2 fichiers.

#### Requis :
* Python3

#### Utilisation :
``` python3 compar.py ```   
Entrez ensuite le nom des fichiers    
Le nombre de lignes entre les blocs correspond au nombre de lignes non similaires maximum entre les blocs (pour pouvoir compter une fonction même si il y plusieurs espaces entre chaque lignes)



