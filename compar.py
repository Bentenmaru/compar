############################### Files Comparator ###############################
# Objective : Compare 2 files to find similarities, the main goal was to find if
#             there was copy/pasted functions in 2 C files
# Author : Nicolas Dejonghe
# Date : 14-06-2017
################################################################################

# Allow Autocompletion of input
import readline
readline.parse_and_bind("tab: complete")

# Define coloration class
class color:
    green = "\033[92m"
    yellow = "\033[93m"
    reset = "\x1b[0m"

# Ask the user to enter the names of files to compare
fname1 = input("Enter the first filename: ")
fname2 = input("Enter the second filename: ")
space = input("Maximum space between 2 lanes in the same block [3]: ") or 3
space = int(space)

# Open file for reading in text mode with encoding ISO-8859-1 to support Hex code
# like 0x00
f1 = open(fname1,encoding="ISO-8859-1")
f2 = open(fname2,encoding="ISO-8859-1")


# Print confirmation
print("-----------------------------------")
print("Comparing files ", " > " + fname1, " < " +fname2, sep='\n')
print("-----------------------------------")

# Create the dict that will stock all similarities found
dictOfValues={}

# Compare each line of f1 to all f2
for line1 in f1:
    # Remove all spaces to avoid similarities with only } or {
    linebis = ''.join(line1.split())

    # Close and open again f2 to reset the file
    f2.close
    f2 = open(fname2,encoding="ISO-8859-1")

    # initialze line number
    line2_no=0

    if ((linebis!='') and (linebis!='{') and (linebis!='}')):

        for line2 in f2:

            line2_no+=1

            #remove trailing spaces
            line1=line1.rstrip()
            line2=line2.rstrip()

            if line1 == line2:
                    # Add the line to dictOfValue to avoid duplicate
                    dictOfValues[line2_no]=line2

# Get the list of indexes sorted so we can display similarities in order
listOfKey = sorted(dictOfValues)
size = len(listOfKey)

# Set variables :
#   Negative prevE to get a positive comparison
#   prevBoo to check if the previous one was similar to the previous previous
prevE = -10
prevBoo = False

for actualE in listOfKey:

    # Get next Key
    index = listOfKey.index(actualE)
    nextE = listOfKey[index+1] if index+1<size else -1

    if actualE-prevE <= space:
        print("\n", color.green+str(actualE)+" ", dictOfValues[actualE], color.reset, end = "")
        prevBoo=True

    else:
        if (nextE-actualE<=space and nextE-actualE>0):
            print("\n", "\n", color.green+str(actualE)+" ", dictOfValues[actualE], color.reset, end = "")
            prevBoo=True
        else:
            if prevBoo:
                print("\n", "\n", str(actualE)+" ",dictOfValues[actualE], end = "\n")
            else:
                print("\n", str(actualE)+" ", dictOfValues[actualE], end = "\n")
            prevBoo = False

    prevE = actualE
print("\n")
# Close the files
f1.close()
f2.close()

##### END #####
